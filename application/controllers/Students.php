<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('student');
        $this->load->model('studentCourse');
        $this->load->model('course');
        $this->load->helper('url_helper');
    }

    public function index()
    {
     	$this->load->library('session');
    	$search = '';
    	if(isset($_REQUEST['search']['value']) && $_REQUEST['search']['value'] !='')
    		$search = $_REQUEST['search']['value'];

        $data['title'] = 'Students';
        $this->load->view('templates/header', $data);
		$this->load->view('students/index', $data);
        $this->load->view('students/student_scripts');
        $this->load->view('templates/footer');
    }

    public function create()
	{
     	$this->load->helper('form');
     	$this->load->library('session');
	    $this->load->library('form_validation');

	    $data['title'] = 'Create a Students';

	    $this->form_validation->set_rules('firstName', 'First Name', 'required');
	    $this->form_validation->set_rules('lastName', 'Last Name', 'required');
	    $this->form_validation->set_rules('email', 'Email', 'required');
	    $this->form_validation->set_rules('class', 'Class', 'required');
	    $this->form_validation->set_rules('phoneNumber', 'Phone Number', 'required');
	    $this->form_validation->set_rules('parentName', 'Parent Name', 'required');

	    if ($this->form_validation->run() === FALSE)
	    {
	        $this->load->view('templates/header', $data);
	        $this->load->view('students/create');
	        $this->load->view('templates/footer');

	    }
	    else
	    {
	        $this->student->create();
	        $this->session->set_flashdata('success_msg', 'Student created successfully');
	       	redirect( base_url() );
	    }
	}

    
    public function edit()
    {
        $id = $this->uri->segment(3);
        if (empty($id))
        {
            show_404();
        }
        
     	$this->load->library('session');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $data['title'] = 'Update Student';
        $data['student'] = $this->student->find($id);

        $this->form_validation->set_rules('firstName', 'First Name', 'required');
        $this->form_validation->set_rules('lastName', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('class', 'Class', 'required');
        $this->form_validation->set_rules('phoneNumber', 'Phone Number', 'required');
        $this->form_validation->set_rules('parentName', 'Parent Name', 'required');
 
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('students/edit', $data);
            $this->load->view('templates/footer');
        }
        else
        {
            $this->student->edit($id);
            $this->session->set_flashdata('success_msg', 'Student updated successfully');
	       	redirect( base_url() );
        }
    }

	public function courses()
	{
		$studentId = $_GET['studentId'];
        $courses = $this->course->findAll();
        $response = $this->studentCourse->find($studentId);
        $assignedCourses=array();
        foreach ($response as $res) {
            $assignedCourses[] = $res->id;
        }
        $data['studentId'] = $studentId;
        $data['assignedCourses'] = $data['notAssignedCourses'] = array();
        foreach ($courses as $key => $course) {
            if(in_array($course->id, $assignedCourses))
                $data['assignedCourses'][]=array('name'=>$course->name, 'id'=>$course->id);
            else
                $data['notAssignedCourses'][]=array('name'=>$course->name, 'id'=>$course->id);
        }
        $this->load->view('students/student_course', $data);
	}
	
	public function studentList()
	{
    	$search = '';
    	if(isset($_REQUEST['search']['value']) && $_REQUEST['search']['value'] !='')
    		$search = $_REQUEST['search']['value'];

       $fetchData = $this->student->makeDatatables();
       $data = array();
       foreach ($fetchData as $key => $row) {
           $subArray = array();
           $subArray[] = $key+1;
           $subArray[] = $row->first_name;
           $subArray[] = $row->last_name;
           $subArray[] = $row->email;
           $subArray[] = $row->class;
           $subArray[] = $row->parent_name;
           $subArray[] = $row->phone_number;
           $subArray[] = '<a href="javascript:void(0)" class="studentCourse" id="'.$row->id.'"><span class="glyphicon glyphicon-book" aria-hidden="true"></span></a>&nbsp;&nbsp;<a href="'.base_url().'index.php/students/edit/'.$row->id.'">edit</a>';
           $data[] = $subArray;
       }

       $outputArray = array(
        "draw" => intval($_POST['draw']),
        "recordsTotal" => $this->student->getAllData(),
        "recordsFiltered" => $this->student->getFilteredData(),
        "data" => $data
       );
       echo json_encode($outputArray);
	}

    public function assignCourse()
    {
        $data = $_POST;
        $res = $this->studentCourse->assignOrRemoveCourse($data);
        if($res)
            echo json_encode(array('status'=>'success', 'data'=>$data));
        else 
            echo json_encode(array('status'=>'error', 'data'=>$data));
    }
}
