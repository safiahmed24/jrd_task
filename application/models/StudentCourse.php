<?php
/**
* Student Model
*/
class StudentCourse extends CI_Model
{
	var $table = 'student_course';

	public function __construct()
	{
		$this->load->database();
	}

	public function find($studentId)
	{
		$this->db->select('courses.id');
	    $this->db->from('student_course');
	    $this->db->join('courses', 'courses.id = student_course.course_id'); 
	    $this->db->where('student_course.student_id='.$studentId);
	    $query = $this->db->get();
	    return $query->result();
	}

	public function assignOrRemoveCourse($data)
	{
        $studentId = $data['studentId'];
        $type = $data['type'];
        $courseId = $data['courseId'];
        if($type=='assigned'){
        	$this -> db -> where('student_id', $studentId);
        	$this -> db -> where('course_id', $courseId);
			return $this -> db -> delete($this->table);
        }else{
        	return $this->db->insert($this->table, array('student_id'=>$studentId, 'course_id'=>$courseId));
        }
    }
}