<?php
/**
* Student Model
*/
class Student extends CI_Model
{
	var $table = 'students';
	var $selectArray = array('id', 'first_name', 'last_name', 'email', 'class', 'parent_name', 'phone_number');
	var $order_column = array('first_name', 'last_name', 'email', 'class', 'parent_name', 'phone_number', null);

	public function makeQuery()
	{
		$this->db->select($this->selectArray);
		$this->db->from($this->table);
		if(isset($_POST['search']['value']))
		{
			$search = $_POST['search']['value'];
			$this->db->like("first_name", $search);
			$this->db->or_like("last_name", $search);
		}
		$this->db->order_by('id', 'desc');
	}

	public function makeDatatables()
	{
		$this->makeQuery();
		if($_POST["length"] != -1)
		{
			$this->db->limit($_POST['length'], $_POST['start']);
		}
		$query = $this->db->get();
		return $query->result();
	}

	public function getFilteredData()
	{
		$this->makeQuery();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function getAllData()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function find($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where('id='.$id);
		$query = $this->db->get();
		return $query->row();
	}
	
	public function __construct()
	{
		$this->load->database();
	}

	public function create()
	{
	    $this->load->helper('url');

	    $data = array(
	        'first_name' => $this->input->post('firstName'),
	        'last_name' => $this->input->post('lastName'),
	        'email' => $this->input->post('email'),
	        'phone_number' => $this->input->post('phoneNumber'),
	        'parent_name' => $this->input->post('parentName'),
	        'class' => $this->input->post('class'),
	    );

	    return $this->db->insert($this->table, $data);
	}

	public function edit($id)
	{
	    $this->load->helper('url');

	    $data = array(
	        'first_name' => $this->input->post('firstName'),
	        'last_name' => $this->input->post('lastName'),
	        'email' => $this->input->post('email'),
	        'phone_number' => $this->input->post('phoneNumber'),
	        'parent_name' => $this->input->post('parentName'),
	        'class' => $this->input->post('class'),
	    );

	    $this->db->where('id='.$id);
	    return $this->db->update($this->table, $data);
	}
}