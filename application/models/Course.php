<?php
/**
* Student Model
*/
class Course extends CI_Model
{
	var $table = 'courses';

	public function __construct()
	{
		$this->load->database();
	}

	public function findAll()
	{
		$this->db->select("*");
		$this->db->from($this->table);
	    $query = $this->db->get();
	    return $query->result();
	}
}