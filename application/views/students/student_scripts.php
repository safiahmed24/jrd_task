<script type="text/javascript">
	$(function(){
		$.fn.dataTable.ext.errMode = 'none';
		$(document).on("click", ".studentCourses", function(e){
			e.preventDefault();
			let studentId = $(this).attr("id");
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>index.php/students/courses",
				dataType: 'json',
				data: {studentId: studentId},
				success: function(res){
					console.log(res)
				}
			})
		})
	})
</script>

