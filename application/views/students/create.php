<div class="container" id="studentCreate">
	<div class="row">
		<h3 class="title"><?php echo $title; ?></h3>
		<a href="<?php echo base_url(); ?>">Student List</a>
	</div>
	<div class="row subContainer">
		<?php echo validation_errors(); ?>
		<div class="col-md-6 pl0">
			<?php $attributes = array('id' => 'studentForm');
			echo form_open('/students/create', $attributes); ?>
				<div class="form-group">
				    <label for="firstName">First Name</label>
				    <input type="text" name="firstName" class="form-control" id="firstName" placeholder="First Name" value="<?php echo set_value('firstName'); ?>">
			  	</div>
			  	<div class="form-group">
				    <label for="lastName">Last Name</label>
				    <input type="text" name="lastName" class="form-control" id="lastName" placeholder="Last Name" value="<?php echo set_value('lastName'); ?>">
			  	</div>
			  	<div class="form-group">
				    <label for="email">Email</label>
				    <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="<?php echo set_value('email'); ?>">
			  	</div>
			  	<div class="form-group">
				    <label for="class">Class</label>
				    <select class="form-control" name="class" id="class`">
				    	<option>1</option>
				    	<option>2</option>
				    	<option>3</option>
				    	<option>4</option>
				    	<option>5</option>
				    </select>
			  	</div>
			  	<div class="form-group">
				    <label for="parentName">Parent Name</label>
				    <input type="text" name="parentName" class="form-control" id="parentName" placeholder="Parent Name" value="<?php echo set_value('parentName'); ?>">
			  	</div>
			  	<div class="form-group">
				    <label for="phoneNumber">Phone Number</label>
				    <input type="number" name="phoneNumber" class="form-control" id="phoneNumber" placeholder="Phone Number" value="<?php echo set_value('phoneNumber'); ?>">
			  	</div>
			    <input type="submit" class="btn btn-default" id="createStudent" name="submit" value="Create Student" />
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		var student = {
			create : function(){
				validationErrors();
			}
		}
		$("#studentForsm").on("submit", function(e){
			e.preventDefault();
			var data = $( "#studentForm" ).serializeArray();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>index.php/students/createStudent",
				dataType: 'json',
				data: {data: $( "#studentForm" ).serialize()},
				success: function(res){
					console.log(res)
				}
			})
		})
	})
</script>