<div class="container" id="studentList">
	<?php if($this->session->flashdata('success_msg')){ ?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('success_msg'); ?>
		</div>
	<?php } ?>
	<div class="row">
		<h3 class="title"><?php echo $title; ?></h3>
		<a href="<?php echo base_url(); ?>index.php/students/create">Create Student</a>
	</div>
	<div class="row subContainer">
		<div class="table-responsive">
			<table class="table table-bordered table-condensed table-striped table-hover studentListTable" id="studentListTable">
				<thead>
					<tr>
						<th> #</th>
						<th> First Name</th>
						<th> Last Name</th>
						<th> Email</th>
						<th> Class</th>
						<th> Parent Name</th>
						<th> Phone Number </th>
						<th> Courses </th>
					</tr>
				</thead>
				<tbody> </tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Assign Course or Remove Course </h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
      
    </div>
</div>
<script type="text/javascript">
	$(function(){
		var dataTable = $("#studentListTable").DataTable({
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: "<?php echo base_url(); ?>index.php/students/studentList",
				type: "POST"
			},
			"columnDefs": [
				{
					"targets": [0,3,4],
					"orderable": false
				}
			]
		});

		var student = {
			studentCourse:  function($studentId){
		        var dataURL = '<?php echo base_url(); ?>index.php/students/courses?studentId='+$studentId;
		        $('.modal-body').load(dataURL,function(){
		            $('#myModal').modal({show:true});
		        });
			}
		}

		$(document).on('click', '.studentCourse',function(e){
			e.preventDefault();
			var $studentId = $(this).attr("id");
			student.studentCourse($studentId);
	    }); 
	})
</script>