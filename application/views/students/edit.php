<div class="container" id="studentCreate">
	<div class="row">
		<h3 class="title"><?php echo $title; ?></h3>
		<a href="<?php echo base_url(); ?>">Student List</a>
	</div>
	<div class="row subContainer">
		<!-- <div class="alert alert-danger hide"> -->
			<?php echo validation_errors(); ?>
		<!-- </div>	 -->
		<div class="col-md-6 pl0">
			<?php $attributes = array('id' => 'studentForm');
			echo form_open('/students/edit/'.$student->id, $attributes); ?>
				<div class="form-group">
				    <label for="firstName">First Name</label>
				    <input type="text" name="firstName" class="form-control" id="firstName" placeholder="First Name" value="<?php echo $student->first_name; ?>">
			  	</div>
			  	<div class="form-group">
				    <label for="lastName">Last Name</label>
				    <input type="text" name="lastName" class="form-control" id="lastName" placeholder="Last Name" value="<?php echo $student->last_name; ?>">
			  	</div>
			  	<div class="form-group">
				    <label for="email">Email</label>
				    <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="<?php echo $student->email; ?>">
			  	</div>
			  	<div class="form-group">
				    <label for="class">Class</label>
				    <select class="form-control" name="class">
				    	<option <?php if($student->class==1) echo 'selected'; ?>>1</option>
				    	<option <?php if($student->class==2) echo 'selected'; ?>>2</option>
				    	<option <?php if($student->class==3) echo 'selected'; ?>>3</option>
				    	<option <?php if($student->class==4) echo 'selected'; ?>>4</option>
				    	<option <?php if($student->class==5) echo 'selected'; ?>>5</option>
				    </select>
			  	</div>
			  	<div class="form-group">
				    <label for="parentName">Parent Name</label>
				    <input type="text" name="parentName" class="form-control" id="parentName" placeholder="Parent Name" value="<?php echo $student->parent_name; ?>">
			  	</div>
			  	<div class="form-group">
				    <label for="phoneNumber">Phone Number</label>
				    <input type="number" name="phoneNumber" class="form-control" id="phoneNumber" placeholder="Phone Number" value="<?php echo $student->phone_number; ?>">
			  	</div>
			    <input type="submit" class="btn btn-default" id="updateStudent" name="submit" value="Update Student" />
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		var student = {
			create : function(){
				validationErrors();
			}
		}
		$("#studentForsm").on("submit", function(e){
			e.preventDefault();
			var data = $( "#studentForm" ).serializeArray();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>index.php/students/createStudent",
				dataType: 'json',
				data: {data: $( "#studentForm" ).serialize()},
				success: function(res){
					console.log(res)
				}
			})
		})
	})
</script>