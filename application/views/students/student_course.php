<h4>Assigned Courses</h4>
<input type="hidden" id="studentId" value="<?= $studentId; ?>" name="">
<table class="table table-bordered table-condensed table-striped table-hover" id="assignedTable">
	<thead>
		<tr>
			<th>#</th>
			<th>Course Name</th>
		</tr>
	</thead>
	<tbody> 
		<?php 
		if(sizeof($assignedCourses) > 0){
		foreach ($assignedCourses as $key => $course) { ?>
			<tr id="course<?= $course['id']; ?>">
				<td> <input type="checkbox" checked="checked" class="courseCheckbox" name="assigned" id="<?php echo $course['id']; ?>"></td>
				<td> <?php echo $course['name']; ?> </td>
			</tr>
		<?php }
			}else{
				?>
				<tr style="text-align: center;" class="noData">
					<td colspan="2">No Course assigned to this Student</td>
				</tr>
				<?php
			} ?>
	</tbody>
</table>
<h4>Not Assigned Courses</h4>
<table class="table table-bordered table-condensed table-striped table-hover" id="notAssignedTable">
	<thead>
		<tr>
			<th>#</th>
			<th>Course Name</th>
		</tr>
	</thead>
	<tbody> 
		<?php 
		if(sizeof($notAssignedCourses) > 0){
		foreach ($notAssignedCourses as $notAssignedCourse) { ?>
			<tr id="course<?= $notAssignedCourse['id']; ?>">
				<td> <input type="checkbox" class="courseCheckbox" name="notassiged" id="<?php echo $notAssignedCourse['id']; ?>"></td>
				<td> <?php echo $notAssignedCourse['name']; ?> </td>
			</tr>
		<?php }
			}else{
				?>
				<tr style="text-align: center;" class="noData">
					<td colspan="2">No Data Found.</td>
				</tr>
				<?php
			} ?>
	</tbody>
</table>
<script type="text/javascript">
	$(function(){
		var student = {
			assignCourse:  function($studentId, $type, $courseId){
		        var dataURL = '<?php echo base_url(); ?>index.php/students/assignCourse';
		        $.ajax({
				type: "POST",
				url: dataURL,
				data: {studentId:$studentId, type:$type, courseId:$courseId},
				dataType: 'json',
				success: function(res){
					// var html = jQuery(jQuery("#course2").detach()).appendTo("table");
					$type = res.data.type;
					$courseId = res.data.courseId;
					var html = $("#course"+$courseId).detach();
					if($type=='assigned'){
						$(html).appendTo($("#notAssignedTable tbody"))
						$('.noData').hide()
						$("#course"+$courseId).find('input').attr("name","notassigned");
					}else{
						$(html).appendTo($("#assignedTable tbody"))
						$('.noData').hide()
						$("#course"+$courseId).find('input').attr("name", "assigned");
					}
				}
			})
			}

		}

		$(document).on('change', '.courseCheckbox',function(e){
			e.preventDefault();
			var $studentId = $("#studentId").val();;
			var $courseId = $(this).attr("id");
			var $type = $(this).attr("name");
			console.log($type);
			student.assignCourse($studentId, $type, $courseId);
	    }); 
	})	

</script>